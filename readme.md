Make sure a commonly readable rendition is available for each file. This makes it easier to share these documents among non-editors and non-professionals.

For example:

- AI -> PDF
- PSD -> JPG / PNG
- SVG -> PNG

